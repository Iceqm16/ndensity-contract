Contract a FCI density matrix to a 2-RDM
by Andrew Valentine
------

compile as "g++ contract.cpp -o contract -llapack" with armadillo installed

run as "./contract inputName nAct nEls nCore D1FileName D2aaFileName D2abFileName"

where nAct is the number of spacial active orbitals, nEls is the number of active electrons, and nCore is the number of core spacial orbitals
