#include <iostream>
#include <cstdlib>
#include <armadillo>
#include <fstream>
#include <string>
#include <cstring>
#include <string.h>
#include <sstream>
#include <iomanip>
#include <math.h>

using namespace std;
using namespace arma;

struct entry
{
  std::string aStr;
  std::string bStr;
  double coef;
};

int nChooseR(int n,int r);
int getIndex(std::string list,int n,int r);
int indexD(int i,int j,int r);
int index(int i,int j,int r);
void buildD2(char *fileName,mat &D1,mat &D2aa,mat &D2ab,int nCore,int nact,int nels);
void resize(mat &D1,mat &D2aa,mat &D2ab,int nact);

int main(int argc, char *argv[])
{
  char source[100];
  //e.g. "logs/ag/ci/" and "agCI", respectively
  strcpy(source,argv[1]);

  int nCore,nact,rank,nels;

  double Ecore,Enuc;

  nact=atoi(argv[2]);
  nels=atoi(argv[3]);
  nCore = atoi(argv[4]);
  rank=nact+nCore;

  mat D1,D2aa,D2ab;

  resize(D1,D2aa,D2ab,rank);

  buildD2(argv[1],D1,D2aa,D2ab,nCore,nact,nels);

  D2aa *= 2.0;
  D2ab *= 2.0;

  ofstream D1File,D2aaFile,D2abFile;
  D1File.open(argv[5]);
  D2aaFile.open(argv[6]);
  D2abFile.open(argv[7]);

  D1File << setprecision(15);
  D2aaFile << setprecision(15);
  D2abFile << setprecision(15);

  for(int i=0;i<D2ab.n_rows;i++)
  {
    for(int j=0;j<D2ab.n_cols;j++)
    {
      D2abFile << D2ab(i,j) << endl;
    }
  }
  for(int i=0;i<D2aa.n_rows;i++)
  {
    for(int j=0;j<D2aa.n_cols;j++)
    {
      D2aaFile << D2aa(i,j) << endl;
    }
  }
  for(int i=0;i<D1.n_rows;i++)
  {
    for(int j=0;j<D1.n_cols;j++)
    {
      D1File << D1(i,j) << endl;
    }
  }

  return EXIT_SUCCESS;
}

int nChooseR(int n,int r)
{
  if(n < 0 || r < 0 || (n-r<0) )
  {
    cout << "invalid nChooseR" << endl;
  }
  int fac=1;
  for(int i=n;i>r;i--)
  {
    fac *= i;
  }
  for(int i=n-r;i>1;i--)
  {
    fac /= i;
  }

  return fac;
}

int getIndex(std::string list,int n,int r)
{
  int x=0;
  int i=0;
  while(n !=r && r != 0)
  {
    if(list[i]=='1')
    {
      x += nChooseR(n-1,r);
      r--;
    }
    n--;
    i++;
  }
  return x;
}

void buildD2(char *fileName,mat &D1,mat &D2aa,mat &D2ab,int nCore,int nact,int nel)
{
  string line,aStr,bStr;
  double coef=0.0;
  double x;
  char c;
  std::vector<entry> list;
  entry tmp;
  int nels=nel/2;
  int rank=D1.n_rows;

  cout << setprecision(8);

  mat M(nChooseR(nact,nels),nChooseR(nact,nels),fill::zeros);
  ifstream theFile;
  theFile.open(fileName);

  theFile >> aStr;
  while( (aStr[0]=='0' || aStr[0]=='1') && !theFile.eof() )
  {
    theFile >> c >> bStr >> c >> coef;
    if(fabs(coef) > pow(10,-10))
    {
      M(getIndex(aStr,nact,nels),getIndex(bStr,nact,nels))=coef;
      tmp.aStr=aStr;
      tmp.bStr=bStr;
      tmp.coef=coef;
      list.push_back(tmp);
    }
    theFile >> aStr;
  }
 
  int num,num2,num3,aKey,bKey;
  string tmpS,tmpS2;

  for(int i=0;i<nCore;i++)
  {
    D1(i,i)=1.0;
    for(int j=0; j<i+1; j++)
    {
      D2ab(i*rank+j,i*rank+j)=0.5;
    }
    for(int j=i+1; j<nCore; j++)
    {
      D2aa(indexD(i,j,rank),indexD(i,j,rank)) += 0.5;
      D2ab(i*rank+j,i*rank+j)=0.5;
    }
  }

  for(int n=0;n<list.size();n++)
  {
    bKey=getIndex(list[n].bStr,nact,nels);
    for(int i=0;i<nact;i++)
    {
      if(list[n].aStr[i]=='1')
      {
	D1(i+nCore,i+nCore) += pow(list[n].coef,2);
	for(int c=0;c<nCore;c++)
	{
	  D2aa(indexD(c,i+nCore,rank),indexD(c,i+nCore,rank))+=0.5*pow(list[n].coef,2);
	  D2ab(index(c,i+nCore,rank),index(c,i+nCore,rank))+=0.5*pow(list[n].coef,2);
	  D2ab(index(i+nCore,c,rank),index(i+nCore,c,rank))+=0.5*pow(list[n].coef,2);
	}//loop over core
	aKey=getIndex(list[n].aStr,nact,nels);
	for(int k=0;k<nact;k++)
	{
	  if(list[n].bStr[k]=='1')
	  {
	    D2ab(index(i+nCore,k+nCore,rank),index(i+nCore,k+nCore,rank))+=0.5*pow(list[n].coef,2);
	    num=0;
	    for(int l=k+1;l<nact;l++)
	    {
	      if(list[n].bStr[l]=='0')
	      {
		tmpS=list[n].bStr;
		tmpS[k]='0'; tmpS[l]='1';
		x = 0.5*list[n].coef*M(aKey,getIndex(tmpS,nact,nels));
		if(num%2==0)
		{
		  D2ab(index(i+nCore,k+nCore,rank),index(i+nCore,l+nCore,rank))+=x;
		  D2ab(index(i+nCore,l+nCore,rank),index(i+nCore,k+nCore,rank))+=x;
		}
		else
		{
		  D2ab(index(i+nCore,k+nCore,rank),index(i+nCore,l+nCore,rank))-=x;
		  D2ab(index(i+nCore,l+nCore,rank),index(i+nCore,k+nCore,rank))-=x;
		}
	      }
	      else
	      {
		num++;
	      }
	    }//loop over l, beta
	  }//k occupied
	}//loop over k, beta
	num=0;
	for(int j=i+1;j<nact;j++)
	{
	  if(list[n].aStr[j]=='1')
	  {
	    D2aa(indexD(i+nCore,j+nCore,rank),indexD(i+nCore,j+nCore,rank))+=0.5*pow(list[n].coef,2);
	    num2=0;
	    for(int l=j+1;l<nact;l++)
	    {
	      if(list[n].aStr[l]=='1')
	      {
		num2++;
	      }//l occupied
	      else
	      {
		tmpS=list[n].aStr;
		tmpS[j]='0'; tmpS[l]='1';
		x = 0.5*list[n].coef*M(getIndex(tmpS,nact,nels),bKey);
		if(num2%2==0)
		{
		  D2aa(indexD(i+nCore,j+nCore,rank),indexD(i+nCore,l+nCore,rank))+=x;
		  D2aa(indexD(i+nCore,l+nCore,rank),indexD(i+nCore,j+nCore,rank))+=x;
		}
		else
		{
		  D2aa(indexD(i+nCore,j+nCore,rank),indexD(i+nCore,l+nCore,rank))-=x;
		  D2aa(indexD(i+nCore,l+nCore,rank),indexD(i+nCore,j+nCore,rank))-=x;
		}
	      }//l unoccupied
	    }//loop over l, k=i
	    for(int k=i+1;k<nact;k++)
	    {
	      if(list[n].aStr[k]=='0' || k==j)
	      {
		num2=0;
		for(int l=k+1;l<nact;l++)
		{
		  if(list[n].aStr[l]=='0' || l==j)
		  {
		    tmpS=list[n].aStr;
		    tmpS[i]='0'; tmpS[j]='0';
		    tmpS[k]='1'; tmpS[l]='1';
		    x = 0.5*list[n].coef*M(getIndex(tmpS,nact,nels),bKey);
		    if( (num+num2)%2==0 )
		    {
		      D2aa(indexD(i+nCore,j+nCore,rank),indexD(k+nCore,l+nCore,rank))+=x;
		      D2aa(indexD(k+nCore,l+nCore,rank),indexD(i+nCore,j+nCore,rank))+=x;
		    }
		    else
		    {
		      D2aa(indexD(i+nCore,j+nCore,rank),indexD(k+nCore,l+nCore,rank))-=x;
		      D2aa(indexD(k+nCore,l+nCore,rank),indexD(i+nCore,j+nCore,rank))-=x;
		    }
		  }//l unoccupied
		  else
		  {
		    num2++;
		  }//l occupied
		}//loop over l
	      }//k unoccupied
	    }//loop over k
	    num++;
	  }//j occupied
	  else
	  {
	    tmpS=list[n].aStr;
	    tmpS[i]='0'; tmpS[j]='1';
	    x = list[n].coef*M(getIndex(tmpS,nact,nels),bKey);
	    if(num%2==0)
	    {
	      D1(i+nCore,j+nCore)+=x;
	      D1(j+nCore,i+nCore)+=x;
	      for(int c=0;c<nCore;c++)
	      {
		D2aa(indexD(c,i+nCore,rank),indexD(c,j+nCore,rank))+=0.5*x;
		D2aa(indexD(c,j+nCore,rank),indexD(c,i+nCore,rank))+=0.5*x;
		D2ab(index(i+nCore,c,rank),index(j+nCore,c,rank))+=0.5*x;
		D2ab(index(j+nCore,c,rank),index(i+nCore,c,rank))+=0.5*x;
		D2ab(index(c,i+nCore,rank),index(c,j+nCore,rank))+=0.5*x;
		D2ab(index(c,j+nCore,rank),index(c,i+nCore,rank))+=0.5*x;
	      }//loop over core
	    }//even permutation
	    else
	    {
	      D1(i+nCore,j+nCore)-=x;
	      D1(j+nCore,i+nCore)-=x;
	      for(int c=0;c<nCore;c++)
	      {
		D2aa(indexD(c,i+nCore,rank),indexD(c,j+nCore,rank))-=0.5*x;
		D2aa(indexD(c,j+nCore,rank),indexD(c,i+nCore,rank))-=0.5*x;
		D2ab(index(i+nCore,c,rank),index(j+nCore,c,rank))-=0.5*x;
		D2ab(index(j+nCore,c,rank),index(i+nCore,c,rank))-=0.5*x;
		D2ab(index(c,i+nCore,rank),index(c,j+nCore,rank))-=0.5*x;
		D2ab(index(c,j+nCore,rank),index(c,i+nCore,rank))-=0.5*x;
	      }//loop over core
	    }//odd permutation
	    num2=0;
	    for(int k=0;k<nact;k++)
	    {
	      if(list[n].bStr[k]=='1')
	      {
		num3=0;
		for(int l=0;l<nact;l++)
		{
		  if(list[n].bStr[l]=='0' || l==k)
		  {
		    tmpS=list[n].aStr;
		    tmpS[i]='0'; tmpS[j]='1';
		    tmpS2=list[n].bStr;
		    tmpS2[k]='0'; tmpS2[l]='1';
		    x=0.5*list[n].coef*M(getIndex(tmpS,nact,nels),getIndex(tmpS2,nact,nels));
		    if( (num+num2+num3)%2==0 )
		    {
		      D2ab(index(i+nCore,k+nCore,rank),index(j+nCore,l+nCore,rank))+=x;
		      D2ab(index(j+nCore,l+nCore,rank),index(i+nCore,k+nCore,rank))+=x;
		    }
		    else
		    {
		      D2ab(index(i+nCore,k+nCore,rank),index(j+nCore,l+nCore,rank))-=x;
		      D2ab(index(j+nCore,l+nCore,rank),index(i+nCore,k+nCore,rank))-=x;
		    }
		  }//l unoccupied
		  else
		  {
		    num3++;
		  }
		}//loop over l
		num2++;
	      }//k occupied
	    }//loop over k
	  }//j unoccupied
        }//j>i
      }//i occupied
    }//loop over i
  }//loop over all determinants


  theFile.close();  
  return;
}

int indexD(int i,int j,int r)
{
  return (i*r - i*(i+3)/2+j-1);
}

int index(int i,int j,int r)
{
  return (i*r+j);
}

void resize(mat &D1,mat &D2aa,mat &D2ab,int nact)
{
  D1.resize(nact,nact);
  D2aa.resize(nact*(nact-1)/2,nact*(nact-1)/2);
  D2ab.resize(pow(nact,2),pow(nact,2));

  return;
}

